class Case{
    constructor(plateau,lettre,chiffre){
        this.lettre = lettre;
        this.chiffre = chiffre;
        this.piece = undefined;
        this.plateau = plateau
    }
    getOwner(){
        if(this.isOccuped())return this.piece.getOwner();
        //else console.log(this.piece)
    }
    addPiece(piece){
        this.piece=piece

    }
    removePiece(){
        tmp = this.occupant;
        this.occupant = undefined;
        return tmp;
    }
    caseAdjacentes(numero){
        let lettre = this.lettre
        let chiffre = this.chiffre;
        switch(numero){
            case 0 :
                lettre ++
                chiffre --
                break;
            case 1 :
                lettre ++
                break;
            case 2 :
                lettre ++
                chiffre ++
                break;
            case 3 :
                chiffre ++
                break;
            case 4 :
                lettre --
                chiffre ++
                break;
            case 5 :
                lettre --
                break;
            case 6 :
                lettre --
                chiffre --
                break;
            case 7 :
                chiffre --
                break;

        }
        //console.log(lettre);
        //console.log(this.plateau);
        if(lettre < 0 || chiffre < 0 || lettre >7 || chiffre >7){
            return undefined;
        }
        return this.plateau[lettre][chiffre]
    }
    updateGrid(plateau){
        this.plateau = plateau
    }
    isOccuped(){
        return (this.piece!=undefined)
    }
    isAttacked(owner){
      let attacker
      let attackerCases=[]
      if(owner==0){attacker=1}
      if(owner==1){attacker=0}
      if (this.plateau!=null){
      for (let j = 0; j < 8; j++) {
        for (let i = 0; i < 8; i++) {
          if(this.plateau[i][j].piece!=undefined && this.plateau[i][j].piece.owner == attacker ){
            attackerCases.push(this.plateau[i][j])
      }
    }
      }
      for(let j=0; j<attackerCases.length;j++){
        for(let i=0;i<attackerCases[j].piece.possibleMove.length;i++){
          if(this.plateau[this.lettre][this.chiffre]==attackerCases[j].piece.possibleMove[i]){
            console.log("case en danger")
            return true
          }
        }
      }
      console.log("case non en danger")
      return false
    }
  }
}
