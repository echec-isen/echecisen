class Fou extends Piece{
    constructor(owner,currentCase,id){
        super( owner,currentCase,id);
        this.nDirection = 4;
    }
    updatePossibleMove(isInEchec){
        this.possibleMove = []
        for(let i=0;i<4;i++){
            this.possibleMove = this.possibleMove.concat(this.getPossibleMoveInDirection(i));
            
        }
    }
    getPossibleMoveInDirection(direction){
        let directionnalMoove = []
        let pointeur = this.currentCase.caseAdjacentes(2*direction);
        while(pointeur!=undefined && !pointeur.isOccuped()){
            directionnalMoove.push(pointeur);
            pointeur = pointeur.caseAdjacentes(2*direction);
        }
        if(pointeur!=undefined && pointeur.getOwner()!=this.owner){//permet de savoir si c'est un ennemi sur la dernière case
            directionnalMoove.push(pointeur);
        
        }
        return directionnalMoove;
    }
    
}
