class Game{
    constructor(){
        this.plateau = new Plateau();
        this.currentPlayer = 0;
        this.turn = 0 ;
        this.depart=undefined
        this.arrivee=undefined
    }
    isFinish(){
        return(this.hasWInner() || this.isPat(this.currentPlayer) || this.isEquality())
    }
    hasWInner(){
        return(this.isFinish() && (this.plateau.isMat(0) || this.plateau.isMat(1)))
    }
    getWinner(){
        if(this.hasWInner() &&  this.plateau.isMat(0))return 0;
        if(this.hasWInner() && this.plateau.isMat(1))return 1;
        else return undefined;
    }
    play(caseDepart,caseArrivee){//retourne true si il joue
        //console.log(caseDepart.getOwner(),this.currentPlayer)
        if(caseDepart.getOwner()==this.currentPlayer){ //check si la pièce appartient au bon joueur
            if(caseDepart.piece.canMove(caseArrivee)){ //check si la pièce peut se déplacer ici
                //console.log("oui")
                this.plateau.updatePlateau(caseDepart,caseArrivee); //change en mémoire ou se trouve les pieces sur les cases
                this.turn ++;
                this.currentPlayer = !this.currentPlayer;
                console.log("la pièce du joueur",caseArrivee.getOwner(),"s'est bien déplacé");
                return true;
            }
            else {
          console.log(caseDepart,"impossible de se déplacer ici")
          this.arrivee=undefined
          this.depart=undefined
          }
        }
        else{
        console.log(caseDepart) ,console.log(" veulliez svp séléctionner une de vos pièce" )
        this.arrivee=undefined
        this.depart=undefined
      }
    }
    isPat(player){
        if(this.plateau.isInEchec(player)){
            return false
        }
        if(this.plateau.pieces[player][4].getPossibleMoove()!=[]){//on fait d'abord cela pour éviter de faire tous les tests précédents à chaque fois et gagner du temps de calcul
            return false
        }
        for(let i=0;i<16;i++){
            if(this.plateau.pieces[player][i].getPossibleMoove()!=[])return false
        }
        return true;
    }
}
