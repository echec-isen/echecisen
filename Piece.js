class Piece{
    constructor( owner,currentCase, id){//on pourrait carrement mettre la case ou il est en parametre
        this.owner = owner        
        this.currentCase = currentCase
        this.possibleMove = [] //tableau des déplacement de la pièce possible
        this.id = id
        this.hasMove = false
        this.isClouted= false //regarde si la piece peut se déplacer ou non(en gros si le fait de déplacer cette piece vous met en échec)
    }
    isEnnemyInCase(numero){ //permet de savoir si une pièce peut être prise par le pion
        //console.log(this.currentCase/*.caseAdjacentes[numero]*/)
        //console.log("numero : ",numero)
        return(this.currentCase.caseAdjacentes(numero) !=undefined 
        && this.currentCase.caseAdjacentes(numero).piece!=undefined 
        && this.currentCase.caseAdjacentes(numero).getOwner()!=this.owner);//return true si il y un adversaire false sinon
    }
    getOwner(){
        return this.owner;
    }
    updatePossibleMovement(casesAcceptables){
        console.log(this.possibleMove,this.id)
        this.possibleMove = this.possibleMove.filter(x => casesAcceptables.includes(x))
        if(this.id==14){
            console.log(casesAcceptables)
            console.log(this.possibleMove)
        }

    }
    getPossibleMove(i){
        if(i != undefined){
            if(i<this.possibleMove.length)return this.possibleMove[i];
            return false
        }
        return this.possibleMove;

    }
    canMove(arrivee){
        this.updatePossibleMove();
        for (let value of this.possibleMove){
            if(value==arrivee)return true
        }
        return false
    }
    isClouted(direction){//on peut réduire cette fonction assez simplement
        let pointeur=this.currentCase.caseAdjacentes(direction)//dans la direction de la pièce adverse
        let newPossibleMove = []
        while(pointeur != undefined){
            for(let value of this.possibleMove){//compare toute les cases de l'ancien tableau avec celle ou la piece empeche toujours l'échec
                if(value == pointeur){
                    newPossibleMove.push(value)
                    break;//gain de temps de calcul
                }
            }
            pointeur=pointeur.caseAdjacentes(direction)
        }
        pointeur=this.currentCase.caseAdjacentes((direction+4)%8)//dans la direction de notre roi;
        while(pointeur != undefined){
            for(let value of this.possibleMove){//compare toute les cases de l'ancien tableau avec celle ou la piece empeche toujours l'échec
                if(value == pointeur){
                    newPossibleMove.push(value)
                    break;//gain de temps de calcul
                }
            }
            pointeur=pointeur.caseAdjacentes((direction+4)%8)
        }
    }
    getEnnemyPossibleMove(){  //en cas de mise en échec par cette pièce renvoie toute les cases où un adversaire
        //pourrait se mettre pour empêcher cette échec
        if(this.nDirection != null){//si il possède cette méthode c'est une tour, reine ou fou

        
            for(let direction = 0;direction<this.nDirection;direction++){
                let a = this.getPossibleMoveInDirection(direction)
                console.log(a.length, a[a.length - 1])
                if(a.length != 0 
                    && a[a.length - 1].getOwner() != this.getOwner()
                    && a[a.length - 1].piece
                    && a[a.length - 1].piece.id ==4){ //alors c'est le roi adverse
                    a.push(this.currentCase)//il peut aussi se déplacer sur notre case(en prenant l'attaquant
                    console.log("please")
                    return a;
                }
            }
        }
        else return [this.currentCase]; //Si ce n'est pas une tour ou .... alors le seul moyen d'empêcher
         //l'echec est de prendre l'attaquant.
        
    }
    
}
