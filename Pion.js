class Pion extends Piece{
    constructor(owner,currentCase,id){
        super(owner,currentCase,id);
    }
    updatePossibleMove(){//remplit le tableau avec tous les mouvement de la pièce possible
        this.possibleMove = []
        for(let i=0;i<2;i++){
            if(this.isEnnemyInCase(4*this.owner+2*i)){//check si dans ses deux diagonales, il y a un ennemi
                this.possibleMove.push(this.currentCase.caseAdjacentes(4*this.owner+2*i));
                //pas encore géré le cas ou l acase adjacente est undefined
            }
        }
        //console.log(this.currentCase);
        let caseAvant =this.currentCase.caseAdjacentes(4*this.owner+1)
        if(caseAvant != undefined && !caseAvant.isOccuped()){
            this.possibleMove.push(caseAvant)
            if(!this.hasMove && !caseAvant.caseAdjacentes(4*this.owner+1).isOccuped())
            this.possibleMove.push(caseAvant.caseAdjacentes(4*this.owner+1));
        }
    }
    
}