class Plateau{
    constructor(){
        this.pieces = []
        this.cases = []
        for(let i = 0;i<8;i++){
            this.cases[i]=new Array(8);
            for (let j=0;j<8;j++){

                this.cases[i][j] = new Case(this.cases,i,j);
            }
        }
        for (let i=0;i<2;i++){ //création d'un tableau à 2 dimensions pour les pièces
            this.pieces[i] = new Array(2);
        }
        for(let i=8;i<16;i++){
            //console.log(this.cases[1][1])
            this.pieces[0][i] = new Pion(0,this.cases[1][i-8],i)
            this.pieces[1][i] = new Pion(1,this.cases[6][i-8],i)

        }
        //console.log(this.pieces[0][9])
        for(let i=0;i<2;i++){
            this.pieces[i][0] = new Tour(i,this.cases[7*i][0],0)
            this.pieces[i][1] = new Cavalier(i,this.cases[7*i][1],1)
            this.pieces[i][2] = new Fou(i,this.cases[7*i][2],2)
            this.pieces[i][3] = new Reine(i,this.cases[7*i][3],3)
            this.pieces[i][4] = new Roi(i,this.cases[7*i][4],4)
            this.pieces[i][5] = new Fou(i,this.cases[7*i][5],5)
            this.pieces[i][6] = new Cavalier(i,this.cases[7*i][6],6)
            this.pieces[i][7] = new Tour(i,this.cases[7*i][7],7)

        }

        //for (let i=0;i<8;i++){//permet d'attribuer chaque cases adjacentes au plateau
            //for (let j=0;i<8;i++)this.cases[i][j].initialize(casesAdjacentes)
        //}
        this.initializePiece();
        for(let i=0;i<8;i++){
            for(let j=0;j<8;j++){
                this.cases[i][j].updateGrid(this.cases);
        }
    }
    this.updatePossibleMove();
}
    initializePiece(){//on peut facilement améliorer cela avec des fonctions
        for(let i=0;i<8;i++){//permet de placer tous les pions au bonne endroit au début de la partie+


            this.cases[1][i].addPiece(this.pieces[0][i+8])//pions du joueur 1
            this.cases[6][i].addPiece(this.pieces[1][i+8])//pions du joueur 2
            this.cases[0][i].addPiece(this.pieces[0][i]) //pièce du joueur 1
            this.cases[7][i].addPiece(this.pieces[1][i]) //pièces du joueur 2

        }
    }
    isInEchec(player){

        for(let piece of this.pieces[(player+1)%2]){
            if(piece != null){
                for(let value of piece.getPossibleMove()){
                    console.log(player)
                    if(piece == this.pieces[1][3])console.log(piece.getPossibleMove(),this.pieces[player][4].currentCase)
                    //if(value == this.pieces[0][])
                    if(this.pieces[player][4].currentCase==value){
                    console.log("true")

                        return true
                    }
                }
            }
        }
        return false

    }
    isMat(player){
        return(this.isInEchec(player) && pieces[player][4].getPossibleMove()==[])
    }
    updateCloutedPiece(){
        for (let player=0;player<2;player++){
            for(let direction = 0;direction<8;direction++){
                let pointeur = this.pieces[player][4].currentCase.caseAdjacentes(direction)
                while(pointeur!= undefined){
                    let tmp = undefined;
                    if(tmp != undefined && pointeur.piece!=undefined ){//si on a une piece en mémoire et que sur la même ligne on en a une autre
                        if(pointeur.piece.owner !=player){//si elle est ennemi notre piece est clouée
                            tmp.piece.isClouted(direction)
                            pointeur=undefined
                        }
                        else pointeur=undefined//si elle est ami il n'y aura aucune case clouée dans cette direction
                    if(pointeur.piece.owner == player)tmp = pointeur.piece;//si on a une de nos piece on la garde en mémoire
                    pointeur = pointeur.caseAdjacentes(direction);
                    }

                }
            }
        }
    }
    updatePlateau(caseDepart,caseArrivee){
        if(caseArrivee.piece!=undefined){
            let id = caseArrivee.piece.id;
            this.pieces[caseArrivee.getOwner()][id]=undefined //le joueur adverse perd une piece
        }
        let id = caseDepart.piece.id;
        let a = this.pieces[caseDepart.getOwner()][id]
        a.hasMove=true;
        a.currentCase = caseArrivee//il faut réussir à link une case et une piece ensemble
        caseDepart.piece=undefined//remet la case de départ vide
        caseArrivee.piece = a
        this.updatePossibleMove(); //permet de changer les déplacements de toutes les pieces du plateau
    }
    updatePossibleMove(){//il faut réussir à retirer les pièces décédées pour continuer à jouer
        console.log(this.isInEchec(0))
        for(let player=0;player<2;player++){
            for (let id=0;id<16;id++){
                if(this.pieces[player][id]!=undefined){//permet de ne plus prendre en compte les pièces dead
                    this.pieces[player][id].updatePossibleMove();
                    
                }
            }
        }
        for(let player=0;player<2;player++){
            if(this.isInEchec(player)){
                console.log("true")
                let piecesEchequantes = this.getPiecesEchequantes(player);
                let protectYourKing = piecesEchequantes[0].getEnnemyPossibleMove()
                for(let piece of piecesEchequantes){ //si jamais il y a plusieurs pièces qui mettent le roi en échec
                    console.log(piece.getEnnemyPossibleMove())
                    protectYourKing = protectYourKing.filter(x => piece.getEnnemyPossibleMove().includes(x));
                    console.log("a")
                }
                for (let id=0;id<16;id++){
                    if(this.pieces[player][id]!=undefined &&
                        id !=4){//permet de ne, ni prendre en compte les pièces dead, ni le roi
                            this.pieces[player][id].updatePossibleMovement(protectYourKing);
                        }
                    if(id==4)this.pieces[player][id].updatePossibleMove();            
                }
            }
        } 
    }

    getPiecesEchequantes(player){
        let piecesEchequantes = [];
        for(let piece of this.pieces[(player+1)%2]){
            
            for(let value of piece.getPossibleMove()){
                if(this.pieces[player][4].currentCase==value){
                    piecesEchequantes.push(piece);
                }
            }
        }
        return piecesEchequantes
    }
}
