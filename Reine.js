class Reine extends Piece{
    constructor(owner, currentCase,id){
        super(owner, currentCase,id);
        this.nDirection = 8;
    }
    updatePossibleMove(){
        this.possibleMove = []
        for(let i=0;i<8;i++){
            this.possibleMove = this.possibleMove.concat(this.getPossibleMoveInDirection(i));
        }
    }
    getPossibleMoveInDirection(direction){
        let pointeur = this.currentCase.caseAdjacentes(direction)
        let directionnalMoove = []
        while(pointeur!=undefined && !pointeur.isOccuped() ){
            directionnalMoove.push(pointeur);
            pointeur = pointeur.caseAdjacentes(direction);
        }
        if(pointeur!=undefined && pointeur.getOwner() != this.owner){//permet de savoir si la dernière case est un ennemi
            directionnalMoove.push(pointeur);
        }
        return directionnalMoove;
    }
}
