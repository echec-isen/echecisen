class Roi extends Piece{
    constructor( owner, currentCase,id){
        super( owner, currentCase,id);
        this.hasRoque = false;
        this.hasMove = false;
        this.possibleMove = []
    }

    updatePossibleMove(plateau){//vérifier si on peut enlever le plateau dans toutes les cases
        this.possibleMove = []
        for(let i=0;i<8;i++){
            let adjacente = this.currentCase.caseAdjacentes(i);
            if(adjacente != undefined && !adjacente.isAttacked(!this.owner)){//si la case adjacente n'est pas protégé par un adversaire;
                if(adjacente.isOccuped()){//si une des case est occupé
                    if(adjacente.getOwner()!=this.owner){//si c'est un ennemi
                        this.possibleMove.push(adjacente) 
                    }
                }
                else{
                    this.possibleMove.push(adjacente);//si la case n'est pas occupé
                }
            }
        }
    }
}