class Tour extends Piece{
    constructor(owner,currentCase,id){
        super(owner,currentCase,id);
        this.nDirection = 4;
    }
    //faire le système de roque
    updatePossibleMove(){
        this.possibleMove = []
        for(let i=0;i<4;i++){
            this.possibleMove = this.possibleMove.concat(this.getPossibleMoveInDirection(i));
        }
    }
    
    getPossibleMoveInDirection(direction){
        let pointeur = this.currentCase.caseAdjacentes(2*direction+1)
        let directionnalMoove = []
        while(pointeur!=undefined && !pointeur.isOccuped() ){
            directionnalMoove.push(pointeur);
            pointeur = pointeur.caseAdjacentes(2*direction+1);
        }
        if(pointeur!=undefined && pointeur.getOwner() != this.owner){//permet de savoir si la dernière case est un ennemi
            directionnalMoove.push(pointeur);
        }
        return directionnalMoove;
    }
    
}