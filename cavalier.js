class Cavalier extends Piece{
    constructor( owner,currentCase,id){
        super( owner,currentCase,id);
    }
    updatePossibleMove(){
        this.possibleMove = []
        for(let i =0;i<4;i++){
            let curseur = this.currentCase.caseAdjacentes(2*i+1);
            for(let j = 0; j<2; j++){
                if(curseur !=undefined){
                    let caseFinale = curseur.caseAdjacentes((2*i+2*j)%8);
                    if(caseFinale != undefined && caseFinale.getOwner()!=this.owner){
                        this.possibleMove.push(caseFinale);
                    }
                }
            }
        }
    }
}