class ChessView {
  constructor(game) {
    this.game = game;
    this.dividende = 0;
    this.reste = 0;
    this.move = undefined;
  }
  initDisplay() {
    
    for (let i = 0; i < 8; i++) {
      for (let j = 0; j < 8; j++) {
        if (i % 2 == 0 && j % 2 != 0) {
          document.getElementsByTagName("td")[8 * i + j].style.backgroundImage = "url('img/blackWood.jpg')";
        }
        else if (i % 2 != 0 && j % 2 == 0) {
          document.getElementsByTagName("td")[8 * i + j].style.backgroundImage = "url('img/blackWood.jpg')";
        }
        else {
          document.getElementsByTagName("td")[8 * i + j].style.backgroundImage = "url('img/whiteWood.jpg')"
        }

        if (game.plateau.cases[i][j].piece != undefined) {
          if (game.plateau.cases[i][j].piece.constructor.name == "Pion" && game.plateau.cases[i][j].piece.owner == 0 && i % 2 == 0 && j % 2 != 0) {
            document.getElementsByTagName("td")[i * 8 + j].style.backgroundImage = "url('img/wP.png'), url('img/blackWood.jpg')";
          }
          else if (game.plateau.cases[i][j].piece.constructor.name == "Pion" && game.plateau.cases[i][j].piece.owner == 0 && i % 2 != 0 && j % 2 == 0) {
            document.getElementsByTagName("td")[i * 8 + j].style.backgroundImage = "url('img/wP.png'), url('img/blackWood.jpg')";
          }
          else if (game.plateau.cases[i][j].piece.constructor.name == "Pion" && game.plateau.cases[i][j].piece.owner == 0) {
            document.getElementsByTagName("td")[i * 8 + j].style.backgroundImage = "url('img/wP.png'), url('img/whiteWood.jpg')";
          }
          if (game.plateau.cases[i][j].piece.constructor.name == "Pion" && game.plateau.cases[i][j].piece.owner == 1 && i % 2 == 0 && j % 2 != 0) {
            document.getElementsByTagName("td")[i * 8 + j].style.backgroundImage = "url('img/bP.png'), url('img/blackWood.jpg')";
          }
          else if (game.plateau.cases[i][j].piece.constructor.name == "Pion" && game.plateau.cases[i][j].piece.owner == 1 && i % 2 != 0 && j % 2 == 0) {
            document.getElementsByTagName("td")[i * 8 + j].style.backgroundImage = "url('img/bP.png'), url('img/blackWood.jpg')";
          }
          else if (game.plateau.cases[i][j].piece.constructor.name == "Pion" && game.plateau.cases[i][j].piece.owner == 1) {
            document.getElementsByTagName("td")[i * 8 + j].style.backgroundImage = "url('img/bP.png'), url('img/whiteWood.jpg')";
          }
          if (game.plateau.cases[i][j].piece.constructor.name == "Tour" && game.plateau.cases[i][j].piece.owner == 0 && i % 2 == 0 && j % 2 != 0) {
            document.getElementsByTagName("td")[i * 8 + j].style.backgroundImage = "url('img/wR.png'), url('img/blackWood.jpg')";
          }
          else if (game.plateau.cases[i][j].piece.constructor.name == "Tour" && game.plateau.cases[i][j].piece.owner == 0 && i % 2 != 0 && j % 2 == 0) {
            document.getElementsByTagName("td")[i * 8 + j].style.backgroundImage = "url('img/wR.png'), url('img/blackWood.jpg')";
          }
          else if (game.plateau.cases[i][j].piece.constructor.name == "Tour" && game.plateau.cases[i][j].piece.owner == 0) {
            document.getElementsByTagName("td")[i * 8 + j].style.backgroundImage = "url('img/wR.png'), url('img/whiteWood.jpg')";
          }
          if (game.plateau.cases[i][j].piece.constructor.name == "Tour" && game.plateau.cases[i][j].piece.owner == 1 && i % 2 == 0 && j % 2 != 0) {
            document.getElementsByTagName("td")[i * 8 + j].style.backgroundImage = "url('img/bR.png'), url('img/blackWood.jpg')";
          }
          else if (game.plateau.cases[i][j].piece.constructor.name == "Tour" && game.plateau.cases[i][j].piece.owner == 1 && i % 2 != 0 && j % 2 == 0) {
            document.getElementsByTagName("td")[i * 8 + j].style.backgroundImage = "url('img/bR.png'), url('img/blackWood.jpg')";
          }
          else if (game.plateau.cases[i][j].piece.constructor.name == "Tour" && game.plateau.cases[i][j].piece.owner == 1) {
            document.getElementsByTagName("td")[i * 8 + j].style.backgroundImage = "url('img/bR.png'), url('img/whiteWood.jpg')";
          }
          if (game.plateau.cases[i][j].piece.constructor.name == "Cavalier" && game.plateau.cases[i][j].piece.owner == 0 && i % 2 == 0 && j % 2 != 0) {
            document.getElementsByTagName("td")[i * 8 + j].style.backgroundImage = "url('img/wN.png'), url('img/blackWood.jpg')";
          }
          else if (game.plateau.cases[i][j].piece.constructor.name == "Cavalier" && game.plateau.cases[i][j].piece.owner == 0 && i % 2 != 0 && j % 2 == 0) {
            document.getElementsByTagName("td")[i * 8 + j].style.backgroundImage = "url('img/wN.png'), url('img/blackWood.jpg')";
          }
          else if (game.plateau.cases[i][j].piece.constructor.name == "Cavalier" && game.plateau.cases[i][j].piece.owner == 0) {
            document.getElementsByTagName("td")[i * 8 + j].style.backgroundImage = "url('img/wN.png'), url('img/whiteWood.jpg')";
          }
          if (game.plateau.cases[i][j].piece.constructor.name == "Cavalier" && game.plateau.cases[i][j].piece.owner == 1 && i % 2 == 0 && j % 2 != 0) {
            document.getElementsByTagName("td")[i * 8 + j].style.backgroundImage = "url('img/bN.png'), url('img/blackWood.jpg')";
          }
          else if (game.plateau.cases[i][j].piece.constructor.name == "Cavalier" && game.plateau.cases[i][j].piece.owner == 1 && i % 2 != 0 && j % 2 == 0) {
            document.getElementsByTagName("td")[i * 8 + j].style.backgroundImage = "url('img/bN.png'), url('img/blackWood.jpg')";
          }
          else if (game.plateau.cases[i][j].piece.constructor.name == "Cavalier" && game.plateau.cases[i][j].piece.owner == 1) {
            document.getElementsByTagName("td")[i * 8 + j].style.backgroundImage = "url('img/bN.png'), url('img/whiteWood.jpg')";
          }
          if (game.plateau.cases[i][j].piece.constructor.name == "Fou" && game.plateau.cases[i][j].piece.owner == 0 && i % 2 == 0 && j % 2 != 0) {
            document.getElementsByTagName("td")[i * 8 + j].style.backgroundImage = "url('img/wB.png'), url('img/blackWood.jpg')";
          }
          else if (game.plateau.cases[i][j].piece.constructor.name == "Fou" && game.plateau.cases[i][j].piece.owner == 0 && i % 2 != 0 && j % 2 == 0) {
            document.getElementsByTagName("td")[i * 8 + j].style.backgroundImage = "url('img/wB.png'), url('img/blackWood.jpg')";
          }
          else if (game.plateau.cases[i][j].piece.constructor.name == "Fou" && game.plateau.cases[i][j].piece.owner == 0) {
            document.getElementsByTagName("td")[i * 8 + j].style.backgroundImage = "url('img/wB.png'), url('img/whiteWood.jpg')";
          }
          if (game.plateau.cases[i][j].piece.constructor.name == "Fou" && game.plateau.cases[i][j].piece.owner == 1 && i % 2 == 0 && j % 2 != 0) {
            document.getElementsByTagName("td")[i * 8 + j].style.backgroundImage = "url('img/bB.png'), url('img/blackWood.jpg')";
          }
          else if (game.plateau.cases[i][j].piece.constructor.name == "Fou" && game.plateau.cases[i][j].piece.owner == 1 && i % 2 != 0 && j % 2 == 0) {
            document.getElementsByTagName("td")[i * 8 + j].style.backgroundImage = "url('img/bB.png'), url('img/blackWood.jpg')";
          }
          else if (game.plateau.cases[i][j].piece.constructor.name == "Fou" && game.plateau.cases[i][j].piece.owner == 1) {
            document.getElementsByTagName("td")[i * 8 + j].style.backgroundImage = "url('img/bB.png'), url('img/whiteWood.jpg')";
          }
          if (game.plateau.cases[i][j].piece.constructor.name == "Roi" && game.plateau.cases[i][j].piece.owner == 0 && i % 2 == 0 && j % 2 != 0) {
            document.getElementsByTagName("td")[i * 8 + j].style.backgroundImage = "url('img/wK.png'), url('img/blackWood.jpg')";
          }
          else if (game.plateau.cases[i][j].piece.constructor.name == "Roi" && game.plateau.cases[i][j].piece.owner == 0 && i % 2 != 0 && j % 2 == 0) {
            document.getElementsByTagName("td")[i * 8 + j].style.backgroundImage = "url('img/wK.png'), url('img/blackWood.jpg')";
          }
          else if (game.plateau.cases[i][j].piece.constructor.name == "Roi" && game.plateau.cases[i][j].piece.owner == 0) {
            document.getElementsByTagName("td")[i * 8 + j].style.backgroundImage = "url('img/wK.png'), url('img/whiteWood.jpg')";
          }
          if (game.plateau.cases[i][j].piece.constructor.name == "Roi" && game.plateau.cases[i][j].piece.owner == 1 && i % 2 == 0 && j % 2 != 0) {
            document.getElementsByTagName("td")[i * 8 + j].style.backgroundImage = "url('img/bK.png'), url('img/blackWood.jpg')";
          }
          else if (game.plateau.cases[i][j].piece.constructor.name == "Roi" && game.plateau.cases[i][j].piece.owner == 1 && i % 2 != 0 && j % 2 == 0) {
            document.getElementsByTagName("td")[i * 8 + j].style.backgroundImage = "url('img/bK.png'), url('img/blackWood.jpg')";
          }
          else if (game.plateau.cases[i][j].piece.constructor.name == "Roi" && game.plateau.cases[i][j].piece.owner == 1) {
            document.getElementsByTagName("td")[i * 8 + j].style.backgroundImage = "url('img/bK.png'), url('img/whiteWood.jpg')";
          }
          if (game.plateau.cases[i][j].piece.constructor.name == "Reine" && game.plateau.cases[i][j].piece.owner == 0 && i % 2 == 0 && j % 2 != 0) {
            document.getElementsByTagName("td")[i * 8 + j].style.backgroundImage = "url('img/wQ.png'), url('img/blackWood.jpg')";
          }
          else if (game.plateau.cases[i][j].piece.constructor.name == "Reine" && game.plateau.cases[i][j].piece.owner == 0 && i % 2 != 0 && j % 2 == 0) {
            document.getElementsByTagName("td")[i * 8 + j].style.backgroundImage = "url('img/wQ.png'), url('img/blackWood.jpg')";
          }
          else if (game.plateau.cases[i][j].piece.constructor.name == "Reine" && game.plateau.cases[i][j].piece.owner == 0) {
            document.getElementsByTagName("td")[i * 8 + j].style.backgroundImage = "url('img/wQ.png'), url('img/whiteWood.jpg')";
          }
          if (game.plateau.cases[i][j].piece.constructor.name == "Reine" && game.plateau.cases[i][j].piece.owner == 1 && i % 2 == 0 && j % 2 != 0) {
            document.getElementsByTagName("td")[i * 8 + j].style.backgroundImage = "url('img/bQ.png'), url('img/blackWood.jpg')";
          }
          else if (game.plateau.cases[i][j].piece.constructor.name == "Reine" && game.plateau.cases[i][j].piece.owner == 1 && i % 2 != 0 && j % 2 == 0) {
            document.getElementsByTagName("td")[i * 8 + j].style.backgroundImage = "url('img/bQ.png'), url('img/blackWood.jpg')";
          }
          else if (game.plateau.cases[i][j].piece.constructor.name == "Reine" && game.plateau.cases[i][j].piece.owner == 1) {
            document.getElementsByTagName("td")[i * 8 + j].style.backgroundImage = "url('img/bQ.png'), url('img/whiteWood.jpg')";
          }
        }
      }
    }
  }
  updateDisplay() {
    for (let i = 0; i < 8; i++) {
      for (let j = 0; j < 8; j++) {
        if (game.plateau.cases[i][j].piece == undefined && document.getElementsByTagName("td")[i * 8 + j].style.background != null) {
          document.getElementsByTagName("td")[i * 8 + j].style.background = "none"
        }
      }
    }
    this.initDisplay()
  }
  Play() {
    let table = document.querySelector('table');
    let rows = document.querySelectorAll('tr');
    let rowsArray = Array.from(rows);

  table.addEventListener('click', (event) => {
    
    if(this.game.depart!=undefined){
      console.log("choisis l'arrivée")
      let rowIndex = rowsArray.findIndex(row => row.contains(event.target));
      let columns = Array.from(rowsArray[rowIndex].querySelectorAll('td'));
      let columnIndex = columns.findIndex(column => column == event.target);
      this.game.arrivee=this.game.plateau.cases[rowIndex][columnIndex]
      console.log(this.game.arrivee)

      }

    if(this.game.depart==undefined ){
      

  let rowIndex = rowsArray.findIndex(row => row.contains(event.target));
  let columns = Array.from(rowsArray[rowIndex].querySelectorAll('td'));
  let columnIndex = columns.findIndex(column => column == event.target);
  this.game.depart=this.game.plateau.cases[rowIndex][columnIndex]
  console.log(this.game.depart.piece);
    if(this.game.depart.getOwner()!=this.game.currentPlayer){
      this.game.depart=undefined
      console.log("Choisis une de tes pièces")
      return
    }
    else{
      //this.game.depart.piece.updatePossibleMove()
      for(let i=0;i<this.game.depart.piece.possibleMove.length;i++){
        console.log(this.findIndex(this.game.plateau.cases, this.game.depart.piece.possibleMove[i]))
        


          }
          this.colorCell()
        }
      }
      if (this.game.depart != undefined && this.game.arrivee != undefined) {
        this.decolorCell()
        game.play(this.game.depart, this.game.arrivee)
        view.updateDisplay()
        game.depart = undefined
        game.arrivee = undefined
      }
    }, false)
  }
  colorCell() {
    for (let i = 0; i < this.game.depart.piece.possibleMove.length; i++) {
      if (this.game.depart.piece.getPossibleMove(i) != false) {
        if (this.game.depart.piece.getPossibleMove(i).getOwner() != undefined) {
          document.getElementsByTagName("td")[
            this.findIndex(this.game.plateau.cases, this.game.depart.piece.possibleMove[i])[0] * 8 +
            this.findIndex(this.game.plateau.cases, this.game.depart.piece.possibleMove[i])[1]].style.borderColor = " rgb(255, 0, 0)"

        } else {
          document.getElementsByTagName("td")[
            this.findIndex(this.game.plateau.cases, this.game.depart.piece.possibleMove[i])[0] * 8 +
            this.findIndex(this.game.plateau.cases, this.game.depart.piece.possibleMove[i])[1]].style.borderColor = " rgba(0, 255, 0, 0.459)"
        }
      }
    }
  }
  decolorCell() {
    for (let i = 0; i < this.game.depart.piece.possibleMove.length; i++) {
      if (this.game.depart.piece.getPossibleMove(i) != false) {
        document.getElementsByTagName("td")[this.findIndex(this.game.plateau.cases, this.game.depart.piece.possibleMove[i])[0] * 8 + this.findIndex(this.game.plateau.cases, this.game.depart.piece.possibleMove[i])[1]].style.borderColor = "black"
      }
    }
  }
  findIndex(array, item) {
    for (let j = 0; j < 8; j++) {
      for (let i = 0; i < 8; i++) {
        if (array[i][j] == item) {
          return [i, j]
        }
      }
    }
    return false
  }

}
