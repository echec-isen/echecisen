//modules
let express = require('express')
let routes = require('./routes')
let user = require('./routes/user')
const socketio = require('socket.io');
let path = require('path');
let session = require('express-session');
var app = require('express')();
var http = require('http').createServer(app);
var io = require('socket.io')(http);
let mysql = require('mysql');
let bodyParser = require("body-parser");
let connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: 'root',
  database: 'test'
});

connection.connect();

global.db = connection;

//tous les environnements
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public')));

app.use(express.static(__dirname + '/views'));

app.use(session({
  secret: 'keyboard cat',
  resave: false,
  saveUninitialized: true,
  cookie: {}
}))

io.on('connection', function(socket){
  console.log("a user is connected")
  let sessionPseudo=require("./routes/user")
  socket.on('disconnect', function(){
    var sql1 = "UPDATE `users` SET `connected`='" + 0 + "' WHERE `pseudo`='" + sessionPseudo + "'";
    db.query(sql1, function (err, result) {
      console.log("a user is disconnected")
   });
    
  });

  socket.on('chat message', function(msg){
    console.log("message reçu : " + msg)
    io.emit('chat message', msg,sessionPseudo);
  });
});



app.get('/', routes.index);//On appelle la page index
app.get('/signup', user.signup);//On appelle la page signup page
app.post('/signup', user.signup);//On appelle la page signup post 
app.get('/login', routes.index);//On appelle la page login page
app.post('/login', user.login);
app.get('/home/accueil', user.accueil);
app.get('/home/logout', user.logout);
//Middleware
http.listen(3000, () => {
  console.log('listening on *:3000');
});

