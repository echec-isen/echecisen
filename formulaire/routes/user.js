const passwordHash = require('password-hash');
//---------------------------------------------Appelle signup------------------------------------------------------
exports.signup = function (req, res) {
   message = '';
   if (req.method == "POST") {
      let post = req.body;
      let pseudo = post.pseudo;
      var password = post.password;
      if (pseudo !== '' && password !== '') {
         password = passwordHash.generate(password);
         var sql1 = "SELECT pseudo FROM `users` WHERE `pseudo`='" + pseudo + "'";
         var query1 = db.query(sql1, function (err, result) {
            if (!result.length) {
               var sql = "INSERT INTO `users`(`pseudo`, `password`, `victoire`, `defaite`, `connected`) VALUES ('" + pseudo + "','" + password + "','" + 0 + "','" + 0 + "','" + 0 + "')";

               var query = db.query(sql, function (err, results) {

                  message = "Compte créé";
                  res.render('signup.ejs', { message: message });
               });
            }else {
               message = "le compte existe déjà";
               res.render('signup', { message: message });
            }

         });

      }
      else {
         message = "Saisie incorrect";
         res.render('signup', { message: message });
      }
   } else {
      res.render('signup');
   }
};

//-----------------------------------------------appelle login------------------------------------------------------
exports.login = function (req, res) {
   var message = '';
   var sess = req.session;


   if (req.method == "POST") {
      var post = req.body;
      var pseudo = post.pseudo;
      var password = post.password;

      var sql = "SELECT id, pseudo, password, connected FROM `users` WHERE `pseudo`='" + pseudo + "'";
      db.query(sql, function (err, results) {
         if (results.length) {
            let hashedPassword = results[0].password;
            if (passwordHash.verify(password, hashedPassword) && results[0].connected == 0) {
               req.session.userId = results[0].id;
               req.session.user = results[0];
               results[0].connected = 1;
               var sql1 = "UPDATE `users` SET `connected`='" + 1 + "' WHERE `id`='" + results[0].id + "'";
               var query = db.query(sql1, function (err, result) {
                  res.redirect('/home/accueil');
               })
            }
            else if (passwordHash.verify(password, hashedPassword) && results[0].connected == 1) {
               message = 'compte déjà connecté';
               res.render('index.ejs', { message: message });
            }
            else {
               message = 'Saisie incorrect';
               res.render('index.ejs', { message: message });
            }
         }
         else {
            message = 'Saisie incorrect';
            res.render('index.ejs', { message: message });
         }

      });
   } else {
      res.render('index.ejs', { message: message });
   }

};
//-----------------------------------------------fonctionnalités accueil----------------------------------------------

exports.accueil = function (req, res, next) {
   sessionPseudo = req.session.user.pseudo;
   module.exports = sessionPseudo;
   var user = req.session.user,
      userId = req.session.userId;
   if (userId == null) {
      res.redirect("/login");
      return;
   }
   var sql = "SELECT * FROM `users` WHERE `id`='" + userId + "'";

   db.query(sql, function (err, result) {
      //var sql1 = "UPDATE `users` SET `socketID`='" + socketID + "' WHERE `id`='" + result[0].id + "'";

      res.render('accueil.ejs', { data: result });

   });
};
//------------------------------------fonctionnalités deconnexion----------------------------------------------
exports.logout = function (req, res) {
   sessionID = req.session.userId;
   req.session.destroy(function (err) {
      var sql1 = "UPDATE `users` SET `connected`='" + 0 + "' WHERE `id`='" + sessionID + "'";
      db.query(sql1, function (err, result) {
         res.redirect("/login");
      });

   })
};
